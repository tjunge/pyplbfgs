#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   wolfe_conditions.py

@author Till Junge <till.junge@altermail.ch>

@date   01 Apr 2021

@brief Implementation of the wolfe conditions and strong wolfe conditions, see
Nocedal "Numerical Optimization", p.34

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.

"""


import numpy as np


class WolfeConditions:
    def __init__(self, c1, c2, fun, grad):
        """
        implements the regular Wolfe conditions, (3.6) in Nocedal "Numerical
        Optimization"

        Keyword Arguments:
        c1   -- sufficient decrease parameter (typically 1e-4, Nocedal p 33)
        c2   -- curvature condition parameter (typically 0.9)
        fun  -- objective function
        grad -- gradient of objective function
        """
        if not (0 < c1 < c2 < 1):
            raise Exception(
                ("Can't use c₁ = {} and c₂ = {}, as they violate the condition "
                 "that 0 < c₁ < c₂ < 1").format(c1, c2))
        self.c1 = c1
        self.c2 = c2
        self.fun = fun
        self.grad = grad

    def sufficient_decrease_criteria(self, x, alpha, p):
        """
        evaluates the decrease and the criterion


        Keyword Arguments:
        x     -- current search position
        alpha -- current step length
        p -- current search direction
        """
        decrease = self.fun(x + alpha*p) - self.fun(x)
        criterion = self.c1 * alpha * self.grad(x).dot(p)
        return decrease, criterion

    def sufficient_decrease(self, x, alpha, p):
        """
        evaluates whether the sufficient decrease condition (3.4) is satisfied.

        Keyword Arguments:
        x     -- current search position
        alpha -- current step length
        p     -- current search direction
        """
        decrease, criterion = self.sufficient_decrease_criteria(x, alpha, p)
        return (decrease <= criterion)

    def curvature_condition_criteria(self, x, alpha, p):
        """
        evaluates the increase in gradient and criteria


        Keyword Arguments:
        x     -- current search position
        alpha -- current step length
        p     -- current search direction
        """
        left = self.grad(x + alpha*p).dot(p)
        right = self.c2 * self.grad(x).dot(p)
        return (left, right)

    def curvature_condition(self, x, alpha, p):
        """
        evaluates whether the curvature condition (3.5) is satisfied

        Keyword Arguments:
        x     -- current search position
        alpha -- current step length
        p     -- current search direction
        """
        left, right = self.curvature_condition_criteria(x, alpha, p)
        return (left >= right)


class StrongWolfeConditions(WolfeConditions):
    def __init__(self, c1, c2, fun, grad):
        """
        implements the strong Wolfe conditions, (3.7) in Nocedal "Numerical
        Optimization"

        Keyword Arguments:
        c1   -- sufficient decrease parameter (typically 1e-4, Nocedal p 33)
        c2   -- curvature condition parameter (typically 0.9)
        fun  -- objective function
        grad -- gradient of objective function
        """
        super().__init__(c1, c2, fun, grad)

    def curvature_condition(self, x, alpha, p):
        """
        evaluates whether the strong curvature condition (3.7b) is satisfied

        Keyword Arguments:
        x     -- current search position
        alpha -- current step length
        p     -- current search direction
        """
        return (abs(self.grad(x + alpha*p).dot(p)) <=
                self.c2 * abs(self.grad(x).dot(p)))
