#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   bfgs.py

@author Till Junge <till.junge@altermail.ch>

@date   27 Apr 2021

@brief  standard BFGS algorithm

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""

from scipy.optimize import line_search
from scipy.optimize.optimize import _line_search_wolfe12

import numpy as np


class BFGS():
    def __init__(self, fun, grad, c1=1e-4, c2=0.9):
        """
        sets up a BFGS solver

        Keyword Arguments:
        fun         -- objective function
        grad        -- gradient of objective function
        c1          -- (default 1e-4) sufficient decrease parameter
        c2          -- (default 0.9) curvature condition parameter
        """
        self.fun = fun
        self.grad = grad
        self.c1 = c1
        self.c2 = c2

    def solve(self, x_0, tol, maxiter=None):
        """
        Keyword Arguments:
        x_0 -- initial guess
        tol -- absolute tolerance
        maxiter -- (default None) by 10 times the system size
        """
        n = x_0.size
        H_k = np.eye(n)
        if maxiter is None:
            maxiter = 20 * n

        x_k = x_0
        f_k = self.fun(x_k)
        df_k = self.grad(x_k)
        # Sets the initial step guess to dx ~ 1
        f_p = f_k + np.linalg.norm(df_k) / 2


        def update(s, y, H):
            rho = 1./y.dot(s)
            Irsy = np.eye(n) - rho * np.outer(s, y)
            rss = rho * np.outer(s, s)

            return Irsy.dot(H.dot(Irsy.T)) + rss

        nfev = 0
        ngev = 0
        for i in range(maxiter):
            p_k = -H_k.dot(df_k)
            alpha_k, fc, gc, f_k, f_p, new_slope = _line_search_wolfe12(
                self.fun, self.grad, xk=x_k, pk=p_k, gfk=df_k, old_fval=f_k,
                old_old_fval=None, c1=self.c1, c2=self.c2,
                amin=1e-100, amax=1e100)
            if alpha_k is None:
                return {"fun": self.fun(x_k),
                        "hess_inv": H_k,
                        "message": "Line search did not  converge",
                        "nfev": nfev,
                        "nit": i+1,
                        "njev": ngev,
                        "status": 1,
                        "success": False,
                        "x": x_k}

            nfev += fc
            ngev += gc
            s_k = alpha_k * p_k
            x_k += s_k
            y_k = -df_k
            df_k = self.grad(x_k)
            y_k += df_k
            H_k[:] = update(s_k, y_k, H_k)
            if np.linalg.norm(df_k) < tol:
                return {"fun": f_k,
                        "hess_inv": H_k,
                        "message": "Optimization terminated successfully",
                        "nfev": nfev,
                        "nit": i+1,
                        "njev": ngev,
                        "status": 0,
                        "success": True,
                        "x": x_k}
            pass
        return {"fun": f_k,
                "hess_inv": H_k,
                "message": "Optimization didn't converge",
                "nfev": nfev,
                "nit": i+1,
                "njev": ngev,
                "status": 1,
                "success": False,
                "x": x_k}
