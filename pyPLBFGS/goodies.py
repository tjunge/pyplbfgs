#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   goodies.py

@author Till Junge <till.junge@altermail.ch>

@date   03 May 2021

@brief  little helpers

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""

import numpy as np
from scipy.sparse.linalg import LinearOperator
from packaging import version
from collections import namedtuple


# definition of an ill-conditioned problem for test purposes
def ill_conditioned_problem(size, condition=False):
    # system matrix
    Q = np.zeros((size, size))
    x0 = np.linspace(0, 1., size, endpoint=False)
    # stiffnesses
    ks = np.ones(size)
    ks[:size//2] *= 5
    ks += np.random.random(ks.size)
    for i in range(size):
        Q[i, (i-1) % size] = -ks[(i-1) % size]
        Q[i, i] = ks[(i-1) % size] + ks[i]
        Q[i, (i+1) % size] = - ks[i]

    Qref = np.zeros((size, size))
    np.fill_diagonal(Qref, 2.)
    np.fill_diagonal(Qref[1:, :-1], -1.)
    np.fill_diagonal(Qref[:-1, 1:], -1.)

    if condition:
        Q[0, 0] += .01
        Qref[0, 0] += 0.01
        if version.parse(np.__version__) >= version.parse("1.17.0"):
            M = np.linalg.pinv(Qref, hermitian=True)
        else:
            M = np.linalg.pinv(Qref)
    else:
        q = np.fft.fft(Qref[:, 0])
        q[1:] = 1./q[1:]
        def fun(x):
            return np.fft.ifft(q*np.fft.fft(x)).real
        M = LinearOperator((size, size), matvec=fun)

    b = - np.einsum("i,ij->j", x0, Q)
    c = .5 * x0.dot(Q.dot(x0))
    def fun(x):
        return .5 * x.dot(Q.dot(x)) + b.dot(x) + c

    def grad(x):
        return Q.dot(x) + b

    problem = namedtuple("problem", ["fun", "grad", "Hess", "M", "solution"])
    return problem(fun, grad, Q, M, x0)
