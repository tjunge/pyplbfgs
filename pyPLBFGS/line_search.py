#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   line_search.py

@author Till Junge <till.junge@altermail.ch>

@date   02 Apr 2021

@brief line search using wolfe conditions, see Nocedal "Numerical Optimization"
algorithm 3.5, p56

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.

"""

import numpy as np

from . import wolfe_conditions


class LineSearch:
    def __init__(self, fun, grad, c1=1e-4, c2=0.9):
        """
        sets up a line search using the wolfe conditions provided

        Keyword Arguments:
        fun   -- objective function
        grad  -- gradient of objective function
        c1    -- (default 1e-4) sufficient decrease parameter
        c2    -- (default 0.9) curvature condition parameter
        """
        self.fun = fun
        self.grad = grad
        self.c1 = c1
        self.c2 = c2

    def zoom(self, phi, dphi, alpha_lo, alpha_hi, maxiter=10):
        """
        implements algorithm 3.6 with bisection

        Keyword Arguments:
        phi      -- objective function along line direction
        dphi     -- objective function gradient along line direction
        alpha_lo -- lower step bound
        alpha_hi -- upper step bund
        maxiter  -- (default 10) maximum number of allowed tries
        """

        phi_0 = phi(0)
        dphi_0 = dphi(0)

        for i in range(maxiter):
            alpha_j = alpha_lo + .1 * (alpha_hi - alpha_lo)
            phi_alpha_j = phi(alpha_j)
            if ((phi_alpha_j > phi_0 + self.c1*alpha_j*dphi_0)
                    or (phi_alpha_j >= phi(alpha_lo))):
                alpha_hi = alpha_j
            else:
                dphi_alpha_j = dphi(alpha_j)
                if abs(dphi_alpha_j) <= -self.c2 * dphi_0:
                    return alpha_j
                if dphi_alpha_j*(alpha_hi-alpha_lo) > 0:
                    aplha_hi = alpha_lo
                alpha_lo = alpha_j
        raise StopIteration("shouldn't have reached here")

    def search(self, x, p, alpha_max, alpha1, maxiter=10):
        """
        performs a line search according to algorithm 3.5

        Keyword Arguments:
        x         -- current search position
        p         -- current search direction
        alpha_max -- maximum step length (currently unused)
        maxiter   -- maximum number of allowed tries
        """
        alpha0 = 0.

        def direction(vec):
            return vec/np.linalg.norm(vec)

        def phi(alpha):
            return self.fun(x + alpha * p)

        def dphi(alpha):
            return self.grad(x + alpha * p).dot(direction(p))

        alpha_p = alpha0  # previous αᵢ₋₁
        alpha_i = alpha1  # current αᵢ
        phi_0 = phi(0)
        phi_i = phi_0   # needed for first update of φᵢ₋₁
        dphi_0 = dphi(0)

        def zoom(a0, a1):
            return self.zoom(phi, dphi, a0, a1)

        for i in range(1, maxiter + 1):
            phi_p = phi_i  # stands for φᵢ₋₁
            phi_i = phi(alpha_i)
            if ((phi_i > phi_0 + self.c1*alpha_i*dphi_0) or
                    (phi_i >= phi_p and i > 1)):
                return zoom(alpha_p, alpha_i)
            dphi_i = dphi(alpha_i)
            if abs(dphi_i) < -self.c2*dphi_0:
                return alpha_i
            if dphi_i >= 0:
                return zoom(alpha_i, alpha_p)
            alpha_i = 10 * alpha_i


            raise StopIteration("Exceeded maxiter")
