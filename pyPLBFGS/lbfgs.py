#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   pbfgs.py

@author Till Junge <till.junge@altermail.ch>

@date   03 May 2021

@brief  LBFGS algorithm with preconditioning

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""
from scipy.optimize.optimize import _line_search_wolfe12, OptimizeResult

import numpy as np




class PLBFGS():
    def __init__(self, fun, grad, preconditioner, m, c1=1e-4, c2=0.9):
        """
        sets up a BFGS solver

        Keyword Arguments:
        fun             -- objective function
        grad            -- gradient of objective function
        preconditioner  -- matrix used to precondition system
        m               -- memory parameter
        c1              -- (default 1e-4) sufficient decrease parameter
        c2              -- (default 0.9) curvature condition parameter
        """
        self.fun = fun
        self.grad = grad
        self.M = preconditioner
        self.m = m
        self.c1 = c1
        self.c2 = c2
        self.steps = list()       # sᵢ in literature
        self.delta_grads = list() # yᵢ in literature

    def two_loop_recursion(self, df_k, gamma):
        """
        Keyword Arguments:
        df_k -- current gradient
        """
        m_k = len(self.steps)
        alphas = np.empty(m_k)
        rhos = np.empty(m_k)

        q = df_k.copy()
        for i in range(m_k-1, -1, -1):
            rhos[i] = 1./self.steps[i].dot(self.delta_grads[i])
            alphas[i] = rhos[i]*self.steps[i].dot(q)
            q -= alphas[i]*self.delta_grads[i]
            pass
        r = gamma*self.M.dot(q)
        for i in range(m_k):
            beta = rhos[i] * self.delta_grads[i].dot(r)
            r += self.steps[i] * (alphas[i] - beta)
            pass
        return r

    def solve(self, x0, tol, maxiter=None, scaling="Nocedal"):
        """
        Keyword Arguments:
        x_0 -- initial guess
        tol -- absolute tolerance
        maxiter -- (default None) by 10 times the system size
        scaling -- (default "Nocedal") can be False, "Nocedal", or "Shanno"
        """

        n = x0.size
        if maxiter is None:
            maxiter = 100 * n

        self.steps = list()
        self.delta_grads = list()

        x_k = x0.copy()
        df_k = self.grad(x_k)
        f_k = self.fun(x_k)

        # Sets the initial step guess to dx ~ 1
        df_p = f_k + np.linalg.norm(df_k) / 2

        nfev = 0
        ngev = 0
        gamma_k = 1.
        for i in range(maxiter):
            p_k = -self.two_loop_recursion(df_k, gamma_k)
            #print(f"step {i:>3}: p · ∇f = {df_k.dot(p_k):+g}, |∇fₖ = {np.linalg.norm(df_k)}|")
            #print(gamma_k * self.M)
            alpha_k, fc, gc, f_k, f_p, new_slope = _line_search_wolfe12(
                self.fun, self.grad, xk=x_k, pk=p_k, gfk=df_k, old_fval=f_k,
                old_old_fval=df_p, c1=self.c1, c2=self.c2)
            if alpha_k is None:
                return OptimizeResult(fun=self.fun(x_k),
                                      message="Line search did not converge",
                                      nfev=nfev,
                                      nit=i+1,
                                      njev=ngev,
                                      status=1,
                                      success=False,
                                      x=x_k)


            nfev += fc
            ngev += gc
            self.steps.append(alpha_k * p_k)
            x_k += self.steps[-1]
            self.delta_grads.append(-df_k)
            df_k[:] = self.grad(x_k)
            self.delta_grads[-1] += df_k
            if np.linalg.norm(df_k) < tol:
                return OptimizeResult(fun=f_k,
                                      message="Optimization terminated successfully",
                                      nfev=nfev,
                                      nit=i+1,
                                      njev=ngev,
                                      status=0,
                                      success=True,
                                      x=x_k)
            if scaling == "Nocedal":
                gamma_k = (
                    self.delta_grads[-1].dot(self.steps[-1]) /
                    self.delta_grads[-1].dot(self.delta_grads[-1]))
            elif scaling == "Shanno":
                gamma_k = (
                    self.delta_grads[-1].dot(self.steps[-1]) /
                    self.delta_grads[-1].dot(self.M.dot(self.delta_grads[-1])))
            elif scaling == False:
                pass
            else:
                raise Exception(f"Unknown scaling type '{scaling}'. Has to be "
                                "False, 'Nocedal', or 'Shanno'")
            if i >= self.m:
                del self.steps[0]
                del self.delta_grads[0]

        return OptimizeResult(fun=f_k,
                              message="Optimization didn't converge",
                              nfev=nfev,
                              nit=i+1,
                              njev=ngev,
                              status=1,
                              success=False,
                              x=x_k)
