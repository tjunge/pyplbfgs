#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   wolfe_conditions_tests.py

@author Till Junge <till.junge@altermail.ch>

@date   01 Apr 2021

@brief  tests for the regular and strong wolfe conditions

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""


from scipy.optimize import rosen, rosen_der, rosen_hess
import pyPLBFGS.wolfe_conditions as wolfe_conditions
import unittest
import numpy as np


class WolfeConditions_check(unittest.TestCase):

    @classmethod
    def fun(cls, x_vec):
        x, y = x_vec
        return np.sin(x) * (y-1)**2

    @classmethod
    def jac(cls, x_vec):
        x, y = x_vec
        return np.array([np.cos(x)*(y-1)**2, 2 * np.sin(x) * (y-1)])

    def setUp(self):
        self.c1 = 1e-4
        self.c2 = 0.9
        self.x0 = np.zeros(2)
        self.wolfe = wolfe_conditions.WolfeConditions(
            self.c1, self.c2, self.fun, self.jac)
        self.strong_wolfe = wolfe_conditions.StrongWolfeConditions(
            self.c1, self.c2, self.fun, self.jac)

    def test_wolfe_conditions(self):
        x = self.x0
        p = -self.jac(x)

        self.assertTrue(self.wolfe.sufficient_decrease(
            x, np.pi/2, p))
        self.assertTrue(self.wolfe.sufficient_decrease(
            x, 1e-5, p))
        self.assertFalse(self.wolfe.sufficient_decrease(
            x, np.pi, p))

        self.assertTrue(self.wolfe.curvature_condition(
            x, np.pi/2, p))
        self.assertFalse(self.wolfe.curvature_condition(
            x, 1e-4, p))

    def test_strong_wolfe_conditions(self):
        x = self.x0
        p = -self.jac(x)

        self.assertTrue(self.strong_wolfe.sufficient_decrease(
            x, np.pi/2, p))
        self.assertTrue(self.strong_wolfe.sufficient_decrease(
            x, 1e-5, p))
        self.assertFalse(self.strong_wolfe.sufficient_decrease(
            x, np.pi, p))

        self.assertTrue(self.strong_wolfe.curvature_condition(
            x, np.pi/2, p))
        self.assertFalse(self.strong_wolfe.curvature_condition(
            x, 1e-4, p))


if __name__ == "__main__":
    unittest.main()
