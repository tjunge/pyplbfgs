#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   line_search_tests.py

@author Till Junge <till.junge@altermail.ch>

@date   02 Apr 2021

@brief  tests for the nocedal line search algo 3.6

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""

from pyPLBFGS import LineSearch
import numpy as np
import unittest


class LineSearch_check(unittest.TestCase):
    @classmethod
    def fun(cls, x_vec):
        x, y = x_vec
        return x**2 - y**2

    @classmethod
    def grad(cls, x_vec):
        x, y = x_vec
        return np.array([2*x, -2*y])

    def setUp(self):
        self.line_search = LineSearch(self.fun, self.grad)

    def test_convex(self):
        search_position = np.array([1, 0.])
        search_direction = -self.grad(search_position)
        alpha_star = self.line_search.search(
            search_position, search_direction, 10, 1)
        self.assertLess(self.fun(search_position + alpha_star *
                                 search_direction), self.fun(search_position))

    def test_concave(self):
        search_position = np.array([0, 1.])
        search_direction = -self.grad(search_position)
        with self.assertRaises(StopIteration):
            self.line_search.search(
                search_position, search_direction, 10, 1)
