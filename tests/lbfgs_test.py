#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   lbfgs_test.py

@author Till Junge <till.junge@altermail.ch>

@date   04 May 2021

@brief  tests for lbfgs

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""

from pyPLBFGS import PLBFGS
from pyPLBFGS.goodies import ill_conditioned_problem

import numpy as np
import scipy
from scipy.optimize import rosen, rosen_der, minimize
from scipy.sparse.linalg import LinearOperator
import unittest


class LBFGS_check(unittest.TestCase):
    def setUp(self):
        self.n = 3
        self.m = 6
        self.solver = PLBFGS(rosen, rosen_der, np.eye(self.n), self.m)
        self.tol = 1e-8

    def test_solve(self):
        search_position = np.linspace(1., 2, self.n)
        myresult = self.solver.solve(search_position, self.tol, maxiter=40)
        print(myresult)
        self.assertTrue(myresult["success"])
        self.assertLess(
            np.linalg.norm(myresult["x"] - np.ones_like(myresult["x"])),
            self.tol)


class PLBFGS_check(unittest.TestCase):
    def setUp(self):
        self.size = 12
        self.m = 4
        self.problem = ill_conditioned_problem(self.size, condition=True)
        self.plbfgs = PLBFGS(self.problem.fun, self.problem.grad, self.problem.M, self.m)
        self.lbfgs = PLBFGS(self.problem.fun, self.problem.grad,
                            LinearOperator((self.size, self.size), lambda x:x), self.m)
        self.tol = 1e-7

    def test_solve(self):
        search_position = np.zeros(self.size)

        sp_result = scipy.optimize.minimize(
            self.problem.fun, search_position, jac=self.problem.grad,
            method="BFGS", tol=self.tol)
        self.assertTrue(sp_result["success"])
        error = np.linalg.norm(self.problem.solution - sp_result["x"])
        self.assertLess(error, np.sqrt(self.tol))

        legacy_result = self.lbfgs.solve(search_position, self.tol)
        self.assertTrue(legacy_result["success"])
        error = np.linalg.norm(self.problem.solution - legacy_result["x"])
        self.assertLess(error, np.sqrt(self.tol))

        result = self.plbfgs.solve(search_position, self.tol)
        error = np.linalg.norm(self.problem.solution - result["x"])
        self.assertLess(error, np.sqrt(self.tol))
