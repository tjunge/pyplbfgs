#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   goodies_test.py

@author Till Junge <till.junge@altermail.ch>

@date   03 May 2021

@brief  testinglittle helpers

Copyright © 2021 Till Junge

pyPLBFGS is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

pyPLBFGS is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with pyPLBFGS; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""

import numpy as np
from pyPLBFGS.goodies import ill_conditioned_problem

import unittest


class IllConditionedProblem_check(unittest.TestCase):
    def test_solution(self):
        size = 5
        problem = ill_conditioned_problem(size)
        tol = 1e-10
        self.assertLess(abs(problem.fun(problem.solution)), tol)
        self.assertLess(np.linalg.norm(problem.grad(problem.solution)), tol)

    def test_approx_solution(self):
        size = 5
        problem = ill_conditioned_problem(size, condition=True)
        tol = 1e-10
        self.assertLess(abs(problem.fun(problem.solution)), tol)
        self.assertLess(np.linalg.norm(problem.grad(problem.solution)), tol)
