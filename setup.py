#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   setup.py

@author Till Junge <till.junge@altermail.ch>

@date   19 Jan 2022

@brief  installation script

@section LICENCE

 Copyright (C) 2022 Till Junge

setup.py is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

setup.py is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""
from setuptools import setup, find_packages

setup(name="pyPLBFGS",
      packages=find_packages(),
      test_suite="tests")
