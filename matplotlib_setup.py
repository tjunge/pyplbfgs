import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rc('font', family='serif', size=8, serif='Computer Modern')#'Times')
mpl.rc('text', usetex=True)
mpl.rc('lines', markersize=2)
mpl.rc('lines', linewidth=.8)
mpl.rc('axes', linewidth=.4)
figure_size = (10/2.54, 7/2.54)
mpl.rc('figure', figsize=figure_size)

mpl.rcParams['text.latex.preamble']=r'''\usepackage{amsmath}
%\usepackage{sansmath}
%\sansmath
%\sffamily
\usepackage{SIunits}'''
mpl.rcParams['legend.fontsize'] = 'small'
